var express = require('express'),
    fs = require('fs'),
    path = require('path'),
    readline = require('readline'),
    request = require('request'),
    cheerio = require('cheerio'),
    app = express(),
    mkdirp = require('mkdirp');
var router = express.Router();


// config vars
var exportBasePath = 'site/source/_posts/';


// this is an article holder
var article = {
    title: "",
    orig_url: "",
    date: "",
    author: "",
    category: "",
    content: "",

    getFileName: function() {
        var title_clean = this.getTitle().replace(/\W/g, '')
        return path.join(exportBasePath, this.category, title_clean) + ".md";
    },

    getContent: function() {
        return "title: " + this.title + "\n" + "orig_url: " + this.orig_url + "\n" + "date: " + this.date + "\n" + "author: " + this.author + "\n" + "category: " + this.category + "\n" + "---" + "\n" + this.content;
    },

    getTitle: function() {
        return this.title.replace(/\s/g, '_');
    },

    export: function() {
        var fn = this.getFileName();
        var a = article;

        console.warn(fn);

        // ensure we got any filename
        if (!fn) {
            console.error('[ERR] Failed to get filename for article!');
            return;
        }

        // ensure export path is exists
        mkdirp(path.dirname(fn), function(err) {
            if (err) {
                console.error('[ERR] Failed to create export folder: ' + fn);
                return;
            }
            else {
                fs.writeFile(fn, a.getContent(), function(err) {
                    if (!err) {
                        console.log('File successfully written! in ' + fn);
                    }
                    else {
                        console.error('[ERR] Failed to export article into file ' + fn);
                    }
                });
            }
        });
    }
};


router.get('/', function(req, res) {
    res.sendfile(path.resolve('../public/index.html'));
});

router.get('/testform', function(req, res) {
    console.log(req.query.action)
   // res.sendfile(path.resolve('../public/index.html'));
});



router.post('/startScrape', function (req, res) {
  //res.send('POST request to the homepage')
  
    
      var url = req.param('url');  
      var category = req.param('category');  
      
      console.log(req)

    request(url, function(error, response, html) {
        article.content = "";
        var $ = cheerio.load(html);
        //if (error) return undefined;

        $('.atitle').filter(function() {
            var data = $(this);
            article.title = data.text();
        });

        $('.date').filter(function() {
            var data = $(this);
            article.date = data.text();
        });

        $('#author_bio_text').filter(function() {
            var data = $(this);
            article.author = data.children().first().children().first().text();

        });

        $('.post').filter(function() {
            var data = $(this);
            var i = 0;

            while (data.children().eq(i)[0] != null && i < 10) {

                if (data.children().eq(i)[0].name == "p") {

                    var line_content = data.children().eq(i).text();

                    if (line_content != undefined && line_content != null) {

                        //replace all sections with multiple spaces with just one space
                        line_content = line_content.replace(/ +(?= )/g, '');

                        //loop string line by line
                        var subArticleFlag = false;

                        var splitStr = line_content.split(/\n/);
                        console.log("Inside post filter, url----: " + splitStr.length);

                        for (var j = 0; j < splitStr.length; j++) {
                            var addLine = true;
                            var tmpLineStr = splitStr[j];
                            //bug fix, filter out entries that aren't valid lines of the article

                            tmpLineStr = tmpLineStr.trim();

                            tmpLineStr = tmpLineStr.replace('\t', '');

                            //check if "var ch_channel" exists
                            var badTextMatchIndex = tmpLineStr.indexOf("var ch_channel");
                            if (badTextMatchIndex != undefined && badTextMatchIndex != null && badTextMatchIndex > -1) {
                                addLine = false;
                            }

                            var badTextMatchIndex = tmpLineStr.indexOf("var _ch");
                            if (badTextMatchIndex != undefined && badTextMatchIndex != null && badTextMatchIndex > -1) {
                                addLine = false;
                            }

                            //remove empty line
                            if (tmpLineStr.length == 0) {
                                addLine = false;
                            }

                            //remove related articles, sub article titles.
                            badTextMatchIndex = tmpLineStr.indexOf("Related Articles");
                            if (badTextMatchIndex != undefined && badTextMatchIndex != null && badTextMatchIndex > -1) {
                                addLine = false;
                                subArticleFlag = true;
                            }

                            //check if line ends with "." and line is under Related Articles
                            if (tmpLineStr.slice(-1) != '.' && subArticleFlag == true) {
                                addLine = false;
                            }
                            else {
                                subArticleFlag = false;
                            }

                            if (addLine) {
                                article.content += tmpLineStr + "\n";
                                //console.log(line_content);
                            }
                        }
                    }
                }
                i++;
            }
        });

        article.category = category;
        article.orig_url = url
        article.export();
    });
  
 
  
})




// generating handler
router.get('/startGenerate', function() {

    // iterate over categories
    fs.readdir(exportBasePath, function(err, dirs) {
        if (err) return;
        dirs.map(function(dir) {
            return path.join(exportBasePath, dir);
        }).filter(function(dir) {
            return fs.statSync(dir).isDirectory();
        }).forEach(function(dir) {
            var d = dir,
                cat = path.basename(d);
            var indexFileName = d + ".md";

            fs.writeFileSync(indexFileName,
                "title: " + cat + "\npagetype: category\n");

            fs.appendFileSync(indexFileName,
                "---\n");

            // iterate over files in given category
            fs.readdir(dir, function(err, files) {
                if (err) return;
                files.forEach(function(file) {
                    var f = path.join(d, file);

                    getTitleFromFile(f, function(err, line) {
                        if (err) {
                            console.error('[ERR] Failed to read title from file ' + fn);
                            return;
                        }


                        fs.appendFileSync(indexFileName,
                            "[" + line + "](/" + cat + "/" + file.substring(0, file.length - 3) + ")\n");
                    });
                });
            });
        });
    });

});


// gets the title (async)
function getTitleFromFile(fn, callback) {

    var rd = readline.createInterface({
        input: fs.createReadStream(fn),
        output: process.stdout,
        terminal: false
    });

    rd.on('line', function(line) {
        if (line.substr(0, 6) == 'title:') {
            callback(null, line.substr(6).trim());
        }
    });
}



module.exports = router;