var express = require('express'),
    app = express()


var server = app.listen(process.env.PORT);
var io = require('socket.io').listen(server);



/**
  * Router
  */

// Error Handling
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
});

module.exports = app;

var routePhotoscraper = require('./routes/photoscraper');
app.use('/photoscraper', routePhotoscraper);

var articlescraper = require('./routes/articlescraper');
app.use('/articlescraper', articlescraper);



var addr = server.address();
console.log("Server listening at", addr.address + ":" + addr.port);