var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app = express();

var server = app.listen('8080')
var io = require('socket.io').listen(server);

io.on('connection', function (socket) {
  	socket.on('startScrape', function(data){
  		//console.log(data);
  		var url = data.url;
  		var filename = data.filename;

		request(url, function(error, response, html){
			    if(!error){
			        var $ = cheerio.load(html);

				    var title, author, date, content = "";
				    //var json = { title : "", author : "", date : "", content: ""};

				    $('.atitle').filter(function(){
				        var data = $(this);
				        title = data.text();            
				        //json.title = title;
				        
				    });

				    $('.date').filter(function(){
				        var data = $(this);
				        date = data.text();
				        //json.date = date;
				    });

				    $('#author_bio_text').filter(function(){
				        var data = $(this);
				        author = data.children().first().children().first().text();
				        //json.date = author;
				    });

				    $('.post').filter(function(){
				        var data = $(this);
				        var i = 0;
				        
				        while(data.children().eq(i)[0] != null && i < 10){
				        	if(data.children().eq(i)[0].name == "p")
				        		content += "</br>" + data.children().eq(i).text();
				        	i++;
				        }
				        
				        //json.content = content;
				    });
				}

			var str = "title: " + title + "\n" + "link: articles/" + filename + ".html" + "\n" + "date: " + date +"\n" + "author: " + author + "\n" + "---" + "\n" + content;
			var path = 'site/source/_posts/';
			fs.writeFile(path + filename + '.md', str, function(err){

			    console.log('File successfully written! in ' + 'projecto/site/source/_posts/');

			});
			var path = 'site/themes/eshopperK/source/articles/';
			str = "<html><head></head>"+ content +"<body></body></html>";

			fs.writeFile(path + filename + '.html', str, function(err){

			});
			
		})
	    
	});
});


app.get('/', function(req, res){

	res.sendfile('public/index.html');

})

var addr = server.address();
console.log("Server listening at", addr.address + ":" + addr.port);
